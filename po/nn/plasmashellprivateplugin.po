# Translation of plasmashellprivateplugin to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2016, 2018, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2023-08-08 19:06+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Fridagar"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Hendingar"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Gjeremål"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Anna"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 hending"
msgstr[1] "%1 hendingar"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "No events"
msgstr "Ingen hendingar"

#: calendar/qml/MonthViewHeader.qml:67
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%1 %2"

#: calendar/qml/MonthViewHeader.qml:114
#, kde-format
msgid "Days"
msgstr "Dagar"

#: calendar/qml/MonthViewHeader.qml:120
#, kde-format
msgid "Months"
msgstr "Månadar"

#: calendar/qml/MonthViewHeader.qml:126
#, kde-format
msgid "Years"
msgstr "År"

#: calendar/qml/MonthViewHeader.qml:164
#, kde-format
msgid "Previous Month"
msgstr "Førre månad"

#: calendar/qml/MonthViewHeader.qml:166
#, kde-format
msgid "Previous Year"
msgstr "Førre år"

#: calendar/qml/MonthViewHeader.qml:168
#, kde-format
msgid "Previous Decade"
msgstr "Førre tiår"

#: calendar/qml/MonthViewHeader.qml:185
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "I dag"

#: calendar/qml/MonthViewHeader.qml:186
#, kde-format
msgid "Reset calendar to today"
msgstr "Gå til i dag"

#: calendar/qml/MonthViewHeader.qml:197
#, kde-format
msgid "Next Month"
msgstr "Neste månad"

#: calendar/qml/MonthViewHeader.qml:199
#, kde-format
msgid "Next Year"
msgstr "Neste år"

#: calendar/qml/MonthViewHeader.qml:201
#, kde-format
msgid "Next Decade"
msgstr "Neste tiår"

#: calendar/qml/MonthViewHeader.qml:257
#, kde-format
msgid "Keep Open"
msgstr "Hald open"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:270
#, kde-format
msgid "Configure…"
msgstr "Set opp …"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Skjermlås er slått på"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Vel om skjermen skal låsast etter vald tid."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Tid før pauseskjermen startar"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Skjermen vert låst etter så mange minutt."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Ny økt"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Filter"

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:148
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:152
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:156
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:161
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with the latest "
"version of Plasma. Please update Plasma in order to use the widget."
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:111
msgctxt "applet category"
msgid "Accessibility"
msgstr "Tilgjenge"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:112
msgctxt "applet category"
msgid "Application Launchers"
msgstr "Programstartarar"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:113
msgctxt "applet category"
msgid "Astronomy"
msgstr "Astronomi"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
msgctxt "applet category"
msgid "Date and Time"
msgstr "Dato og klokkeslett"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
msgctxt "applet category"
msgid "Development Tools"
msgstr "Utviklingsverktøy"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:116
msgctxt "applet category"
msgid "Education"
msgstr "Utdanning"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:117
msgctxt "applet category"
msgid "Environment and Weather"
msgstr "Vêr og miljø"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
msgctxt "applet category"
msgid "Examples"
msgstr "Eksempel"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:119
msgctxt "applet category"
msgid "File System"
msgstr "Filsystem"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:120
msgctxt "applet category"
msgid "Fun and Games"
msgstr "Spel og moro"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
msgctxt "applet category"
msgid "Graphics"
msgstr "Bilete"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
msgctxt "applet category"
msgid "Language"
msgstr "Språk"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:123
msgctxt "applet category"
msgid "Mapping"
msgstr "Kart"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:124
msgctxt "applet category"
msgid "Miscellaneous"
msgstr "Ymse"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
msgctxt "applet category"
msgid "Multimedia"
msgstr "Multimedia"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:126
msgctxt "applet category"
msgid "Online Services"
msgstr "Nett-tenester"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:127
msgctxt "applet category"
msgid "Productivity"
msgstr "Produktivitet"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:128
msgctxt "applet category"
msgid "System Information"
msgstr "Systeminformasjon"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:129
msgctxt "applet category"
msgid "Utilities"
msgstr "Verktøy"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:130
msgctxt "applet category"
msgid "Windows and Tasks"
msgstr "Vindauge og oppgåver"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:131
msgctxt "applet category"
msgid "Clipboard"
msgstr "Utklippstavle"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:132
msgctxt "applet category"
msgid "Tasks"
msgstr "Oppgåver"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:148
#, kde-format
msgid "All Widgets"
msgstr "Alle element"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:152
#, kde-format
msgid "Running"
msgstr "Element som køyrer"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:158
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Avinstallerbare element"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:162
#, kde-format
msgid "Categories:"
msgstr "Kategoriar:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:232
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Last ned nye skjermelement"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:241
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Installer skjermelement frå lokal fil …"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:503
#, kde-format
msgid "Select Plasmoid File"
msgstr "Vel skjermelement-fil"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installing the package %1 failed."
msgstr "Feil ved installering av pakken %1."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installation Failure"
msgstr "Mislukka installering"
