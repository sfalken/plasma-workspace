# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Diego Iastrubni <elcuco@kde.org>, 2008, 2012, 2013.
# Tahmar1900 <tahmar1900@gmail.com>, 2011.
# elkana bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
# SPDX-FileCopyrightText: 2023 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.battery\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:39+0000\n"
"PO-Revision-Date: 2023-11-20 14:35+0200\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Lokalize 23.08.3\n"

#: package/contents/ui/BatteryItem.qml:107
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:171
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr "בריאות הסוללה עומדת על %1% וכדאי להחליפה. נא ליצור קשר עם היצרן."

#: package/contents/ui/BatteryItem.qml:191
#, kde-format
msgid "Time To Full:"
msgstr "זמן לסיום הטעינה:"

#: package/contents/ui/BatteryItem.qml:192
#, kde-format
msgid "Remaining Time:"
msgstr "זמן שנותר:"

#: package/contents/ui/BatteryItem.qml:197
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr "מתבצע שערוך…"

#: package/contents/ui/BatteryItem.qml:209
#, kde-format
msgid "Battery Health:"
msgstr "בריאות הסוללה:"

#: package/contents/ui/BatteryItem.qml:215
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:229
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "הסוללה מוגדרת להיטען עד %1% לכל היותר."

#: package/contents/ui/CompactRepresentation.qml:105
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/logic.js:23 package/contents/ui/logic.js:29
#: package/contents/ui/main.qml:142
#, kde-format
msgid "Fully Charged"
msgstr "טעונה במלואה"

#: package/contents/ui/logic.js:28
#, kde-format
msgid "Discharging"
msgstr "נפרקת"

#: package/contents/ui/logic.js:30
#, kde-format
msgid "Charging"
msgstr "בטעינה"

#: package/contents/ui/logic.js:32
#, kde-format
msgid "Not Charging"
msgstr "לא בטעינה"

#: package/contents/ui/logic.js:35
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "לא מחוברת"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Power and Battery"
msgstr "צריכת חשמל וסוללה"

#: package/contents/ui/main.qml:117 package/contents/ui/main.qml:286
#, kde-format
msgid "Power Management"
msgstr "ניהול צריכת חשמל"

#: package/contents/ui/main.qml:149
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "סוללה על %1%, לא נטענת"

#: package/contents/ui/main.qml:151
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "סוללה על %1%, מחוברת לחשמל אך עדיין נפרקת"

#: package/contents/ui/main.qml:153
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "סוללה על %1%, נטענת"

#: package/contents/ui/main.qml:156
#, kde-format
msgid "Battery at %1%"
msgstr "הסוללה טעונה ב־%1%"

#: package/contents/ui/main.qml:164
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr "המטען לא מספיק חזק כדי לטעון את הסוללה"

#: package/contents/ui/main.qml:168
#, kde-format
msgid "No Batteries Available"
msgstr "סוללה לא זמינה"

#: package/contents/ui/main.qml:174
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "%1 עד לטעינה מלאה"

#: package/contents/ui/main.qml:176
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "נותרו %1"

#: package/contents/ui/main.qml:179
#, kde-format
msgid "Not charging"
msgstr "לא נטענת"

#: package/contents/ui/main.qml:183
#, kde-format
msgid "Automatic sleep and screen locking are disabled"
msgstr "שינה ונעילת מסך אוטומטית מושבתות"

#: package/contents/ui/main.qml:188
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] "יישום ביקש להפעיל מצב ביצועים משופרים"
msgstr[1] "שני יישומים ביקשו להפעיל מצב ביצועים משופרים"
msgstr[2] "%1 יישומים ביקשו להפעיל מצב ביצועים משופרים"
msgstr[3] "%1 יישומים ביקשו להפעיל מצב ביצועים משופרים"

#: package/contents/ui/main.qml:192
#, kde-format
msgid "System is in Performance mode"
msgstr "המערכת במצב ביצועים משופרים"

#: package/contents/ui/main.qml:196
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] "יישום ביקש להפעיל מצב חיסכון בחשמל"
msgstr[1] "שני יישומים ביקשו להפעיל מצב חיסכון בחשמל"
msgstr[2] "%1 יישומים ביקשו להפעיל מצב חיסכון בחשמל"
msgstr[3] "%1 יישומים ביקשו להפעיל מצב חיסכון בחשמל"

#: package/contents/ui/main.qml:200
#, kde-format
msgid "System is in Power Save mode"
msgstr "המערכת במצב חיסכון בחשמל"

#: package/contents/ui/main.qml:260
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "יישומון הסוללה מאפשר בלימה כלל מערכתית"

#: package/contents/ui/main.qml:299
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "הפעלת מצב %1 נכשלה"

#: package/contents/ui/main.qml:311
#, kde-format
msgid "&Show Energy Information…"
msgstr "ה&צגת פרטי אנרגיה…"

#: package/contents/ui/main.qml:317
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr "הצגת אחוזי הטעונה על הסמל כשהסוללה טעונה חלקית"

#: package/contents/ui/main.qml:329
#, kde-format
msgid "&Configure Energy Saving…"
msgstr "הגדרת חיסכון ב&אנרגיה…"

#: package/contents/ui/PowerManagementItem.qml:41
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr "לחסום שינה ונעילת מסך ידנית"

#: package/contents/ui/PowerManagementItem.qml:76
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"המחשב הנייד שלך מוגדר לא לישון כשהמסך הפנימי נסגר בזמן שמחובר צג חיצוני."

#: package/contents/ui/PowerManagementItem.qml:87
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] "יישום אחד חוסם כרגע שינה ונעילת מסך:"
msgstr[1] "שני יישומים חוסמים כרגע שינה ונעילת מסך:"
msgstr[2] "%1 יישומים חוסמים כרגע שינה ונעילת מסך:"
msgstr[3] "%1 יישומים חוסמים כרגע שינה ונעילת מסך:"

#: package/contents/ui/PowerManagementItem.qml:107
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr "%1 חוסם כרגע שינה ונעילת מסך (%2)"

#: package/contents/ui/PowerManagementItem.qml:109
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr "%1 חוסם כרגע שינה ונעילת מסך (מסיבה לא ידועה)"

#: package/contents/ui/PowerManagementItem.qml:111
#, kde-format
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr "יישום חוסם כרגע שינה ונעילת מסך (%1)"

#: package/contents/ui/PowerManagementItem.qml:113
#, kde-format
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr "יישום חוסם כרגע שינה ונעילת מסך (מסיבה לא ידועה)"

#: package/contents/ui/PowerManagementItem.qml:117
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:119
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: סיבה לא ידועה"

#: package/contents/ui/PowerManagementItem.qml:121
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr "יישום לא ידוע: %1"

#: package/contents/ui/PowerManagementItem.qml:123
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr "יישום לא ידוע: סיבה לא ידועה"

#: package/contents/ui/PowerProfileItem.qml:37
#, kde-format
msgid "Power Save"
msgstr "חיסכון בחשמל"

#: package/contents/ui/PowerProfileItem.qml:41
#, kde-format
msgid "Balanced"
msgstr "מאוזן"

#: package/contents/ui/PowerProfileItem.qml:45
#, kde-format
msgid "Performance"
msgstr "ביצועים משופרים"

#: package/contents/ui/PowerProfileItem.qml:62
#, kde-format
msgid "Power Profile"
msgstr "פרופיל צריכת חשמל"

#: package/contents/ui/PowerProfileItem.qml:97
#, kde-format
msgctxt "Power profile"
msgid "Not available"
msgstr "לא זמין"

#: package/contents/ui/PowerProfileItem.qml:192
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"מצב ביצועים משופרים הושבת כדי להפחית את החום שנוצר כיוון שהמחשב זיהה שהוא "
"כנראה על הירכיים שלך."

#: package/contents/ui/PowerProfileItem.qml:194
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr "מצב ביצועים משופרים לא זמין כיוון שהמחשב שלך חם מדי."

#: package/contents/ui/PowerProfileItem.qml:196
#, kde-format
msgid "Performance mode is unavailable."
msgstr "מצב ביצועים משופרים אינו זמין."

#: package/contents/ui/PowerProfileItem.qml:209
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"הביצועים כנראה מצומצמים כדי להפחית את החום שנוצר כיוון שהמחשב זיהה שהוא "
"כנראה על הירכיים שלך."

#: package/contents/ui/PowerProfileItem.qml:211
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr "הביצועים כנראה מצומצמים כיוון שהמחשב חם מדי."

#: package/contents/ui/PowerProfileItem.qml:213
#, kde-format
msgid "Performance may be reduced."
msgstr "הביצועים כנראה מצומצמים."

#: package/contents/ui/PowerProfileItem.qml:224
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "יישום אחד ביקש להפעיל %2:"
msgstr[1] "שני יישומים ביקשו להפעיל %2:"
msgstr[2] "%1 יישומים ביקשו להפעיל %2:"
msgstr[3] "%1 יישומים ביקשו להפעיל %2:"

#: package/contents/ui/PowerProfileItem.qml:242
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerProfileItem.qml:261
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""
"יכול להיות שיש תמיכה בפרופילי צריכת חשמל במכשיר שלך.<nl/>כדאי לנסות להתקין "
"את החבילה <command>power-profiles-daemon</command> בעזרת מנהל החבילות של "
"ההפצה שלך ולהפעיל את המערכת מחדש."

#~ msgctxt "Placeholder is brightness percentage"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery and Brightness"
#~ msgstr "סוללה ובהירות"

#~ msgid "Brightness"
#~ msgstr "בהירות"

#~ msgid "Battery"
#~ msgstr "סוללה"

#~ msgid "Scroll to adjust screen brightness"
#~ msgstr "גלילה כאן תכוון את בהירות המסך"

#~ msgid "Display Brightness"
#~ msgstr "בהירות תצוגה"

#~ msgid "Keyboard Brightness"
#~ msgstr "בהירות מקלדת"

#, fuzzy
#~| msgid "Power management is disabled"
#~ msgid "Performance mode has been manually enabled"
#~ msgstr "ניהול צריכת החשמל כבוי:"

#~ msgid "General"
#~ msgstr "כללי"

#, fuzzy
#~| msgctxt "short symbol to signal there is no battery curently available"
#~| msgid "-"
#~ msgctxt "short symbol to signal there is no battery currently available"
#~ msgid "-"
#~ msgstr "-"

#~ msgid "Configure Power Saving..."
#~ msgstr "הגדר חיסכון באנרגיה..."

#~ msgid "Time To Empty:"
#~ msgstr "זמן נותר לשימוש:"

#~ msgid ""
#~ "Disabling power management will prevent your screen and computer from "
#~ "turning off automatically.\n"
#~ "\n"
#~ "Most applications will automatically suppress power management when they "
#~ "don't want to have you interrupted."
#~ msgstr ""
#~ "ביטול מנהל צריכת החשמל יגרום למסך ולמחשב לא להתכונן אוטומטית.\n"
#~ "\n"
#~ "רוב הישומים ימנעו לבד את ניהול צריכת החשמל שהם לא מעוניינים שתפריע להם"

#~ msgctxt "Some Application and n others are currently suppressing PM"
#~ msgid ""
#~ "%2 and %1 other application are currently suppressing power management."
#~ msgid_plural ""
#~ "%2 and %1 other applications are currently suppressing power management."
#~ msgstr[0] "%2 ועוד %1 יישומים השהו את ניהול צריכת החשמל."
#~ msgstr[1] "%2 ועוד %1 יישומים השהו את ניהול צריכת החשמל."

#~ msgctxt "Some Application is suppressing PM"
#~ msgid "%1 is currently suppressing power management."
#~ msgstr "%1 כרגע השהה את ניהול צריכת החשמל."

#~ msgctxt "Some Application is suppressing PM: Reason provided by the app"
#~ msgid "%1 is currently suppressing power management: %2"
#~ msgstr "%1 כרגע השהה את ניהול צריכת החשמל: %2"

#~ msgctxt "Used for measurement"
#~ msgid "100%"
#~ msgstr "100%"

#, fuzzy
#~| msgid "%1%. Charging"
#~ msgid "%1% Charging"
#~ msgstr "%1%. נטען"

#, fuzzy
#~| msgid "%1%. Plugged in"
#~ msgid "%1% Plugged in"
#~ msgstr "%1%. מחובר"

#~ msgid "%1% Battery Remaining"
#~ msgstr "%1% אחוזי סוללה נותרו"

#, fuzzy
#~| msgid "Capacity:"
#~ msgctxt "The degradation in the battery's energy capacity"
#~ msgid "Capacity degradation:"
#~ msgstr "כמות:"

#, fuzzy
#~| msgctxt "Placeholder is battery percentage"
#~| msgid "%1%"
#~ msgctxt "Placeholder is battery's capacity degradation"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgctxt "Placeholder is battery capacity"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Vendor:"
#~ msgstr "ספק:"

#~ msgid "Model:"
#~ msgstr "מודל:"
