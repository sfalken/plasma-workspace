# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# SPDX-FileCopyrightText: 2023 Vincenzo Reale <smart2128vr@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-15 01:54+0000\n"
"PO-Revision-Date: 2023-09-17 10:10+0200\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.1\n"

#: multiplexermodel.cpp:71
#, kde-format
msgctxt "@action:button"
msgid "Choose player automatically"
msgstr "Scegli automaticamente il lettore"
