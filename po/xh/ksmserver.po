# translation of ksmserver.po to Xhosa
# K Desktop Environment - ksmserver
# Copyright (C) 2001 translate.org.za
# Antoinette Dekeni <antoinette@transalate.org.za>, 2001.
# Lwandle Mgidlana <lwandle@translate.org.za>, 2002
#
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2002-10-26 10:42SAST\n"
"Last-Translator: Lwandle Mgidlana <lwandle@translate.org.za>\n"
"Language-Team: Xhosa <xhosa@translate.org.za>\n"
"Language: xh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.0beta2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: logout.cpp:273
#, kde-format
msgid "Logout canceled by '%1'"
msgstr ""

#: main.cpp:67
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:71 main.cpp:80
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:74
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:82
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:87
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:90
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:104 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:106 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:116 main.cpp:130
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:119 main.cpp:133
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:150
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:153
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:160
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:194
#, fuzzy, kde-format
#| msgid ""
#| "The reliable KDE session manager that talks the standard X11R6 \n"
#| "session management protocol (XSMP)."
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"Umphathi wentlanganiso ye KDE othetha ngomgangatho X11R6 \n"
"intlanganiso yophatho yomthetho ozakulandelwa (XSMP)."

#: main.cpp:198
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Igcina kwakhona intlanganiso yomsebenzisi egciniweyo ukuba ikhona"

#: main.cpp:201
#, fuzzy, kde-format
msgid "Also allow remote connections"
msgstr "Ivumela futhi udibaniso lobeko kude."

#: main.cpp:204
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:208
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:813
#, kde-format
msgctxt "@label an unknown executable is using resources"
msgid "[unknown]"
msgstr ""

#: server.cpp:836
#, kde-kuit-format
msgctxt "@label notification; %1 is a list of executables"
msgid ""
"Unable to manage some apps because the system's session management resources "
"are exhausted. Here are the top three consumers of session resources:\n"
"%1"
msgstr ""

#: server.cpp:1108
#, kde-kuit-format
msgctxt "@label notification; %1 is an executable name"
msgid ""
"Unable to restore <application>%1</application> because it is broken and has "
"exhausted the system's session restoration resources. Please report this to "
"the app's developers."
msgstr ""

#, fuzzy
#~| msgid "The KDE Session Manager"
#~ msgid "Session Management"
#~ msgstr "Umphathi Wentlanganiso ye KDE"

#, fuzzy
#~| msgid ""
#~| "Starts 'wm' in case no other window manager is \n"
#~| "participating in the session. Default is 'kwin'"
#~ msgid ""
#~ "Starts <wm> in case no other window manager is \n"
#~ "participating in the session. Default is 'kwin'"
#~ msgstr ""
#~ "Iqala 'wm' akukho mphathi we window ongomnye \n"
#~ "uzakuthatha inxaxheba entlanganisweni. Engagqibekanga yi 'kwin'"

#, fuzzy
#~ msgid "Sleeping in 1 second"
#~ msgid_plural "Sleeping in %1 seconds"
#~ msgstr[0] "&Cima i Khompyutha"
#~ msgstr[1] "&Cima i Khompyutha"

#, fuzzy
#~ msgid "Logging out in 1 second."
#~ msgid_plural "Logging out in %1 seconds."
#~ msgstr[0] "&Cima i Khompyutha"
#~ msgstr[1] "&Cima i Khompyutha"

#, fuzzy
#~ msgid "Turning off computer in 1 second."
#~ msgid_plural "Turning off computer in %1 seconds."
#~ msgstr[0] "&Cima i Khompyutha"
#~ msgstr[1] "&Cima i Khompyutha"

#, fuzzy
#~ msgid "Restarting computer in 1 second."
#~ msgid_plural "Restarting computer in %1 seconds."
#~ msgstr[0] "&Cima i Khompyutha"
#~ msgstr[1] "&Cima i Khompyutha"

#, fuzzy
#~ msgid "Turn Off Computer"
#~ msgstr "&Cima i Khompyutha"

#, fuzzy
#~ msgid "Restart Computer"
#~ msgstr "&Qala kwakhona i khompyutha"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Lwandle Mgidlana"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "lwandle@translate.org.za"

#, fuzzy
#~ msgid "End Session for %1"
#~ msgstr "Ukuphela Kwentlanganiso ye  \"%1\""

#, fuzzy
#~ msgid "End Session for %1 (%2)"
#~ msgstr "Ukuphela Kwentlanganiso ye  \"%1\""

#, fuzzy
#~ msgid "End Current Session"
#~ msgstr "Ukuphela Kwentlanganiso ye  \"%1\""
