# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2014, 2020, 2021, 2022, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2024-01-30 08:31+0100\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "Disposition de Claviero: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "Nomine usator"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "Contrasigno"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "Accede"

#: ../sddm-theme/Main.qml:200 contents/lockscreen/LockScreenUi.qml:276
#, kde-format
msgid "Caps Lock is on"
msgstr "Caps Lock es active"

#: ../sddm-theme/Main.qml:212 ../sddm-theme/Main.qml:356
#: contents/logout/Logout.qml:200
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Dormi"

#: ../sddm-theme/Main.qml:219 ../sddm-theme/Main.qml:363
#: contents/logout/Logout.qml:225 contents/logout/Logout.qml:238
#, kde-format
msgid "Restart"
msgstr "Re-Initia"

#: ../sddm-theme/Main.qml:226 ../sddm-theme/Main.qml:370
#: contents/logout/Logout.qml:251
#, kde-format
msgid "Shut Down"
msgstr "Arresto (shut down)"

#: ../sddm-theme/Main.qml:233
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Altere..."

#: ../sddm-theme/Main.qml:342
#, kde-format
msgid "Type in Username and Password"
msgstr "Typa le nomine de usator e le contrasigno"

#: ../sddm-theme/Main.qml:377
#, kde-format
msgid "List Users"
msgstr "Lista usatores"

#: ../sddm-theme/Main.qml:452 contents/lockscreen/LockScreenUi.qml:363
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Claviero virtual"

#: ../sddm-theme/Main.qml:526
#, kde-format
msgid "Login Failed"
msgstr "Accesso falleva"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "Session de scriptorio: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "Clock (Horologio):"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "Mantene visibile quando le prompt disblocante dispare"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "Controlos de Multimedia:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "Monstra sub prompt disblocante"

#: contents/lockscreen/LockScreenUi.qml:50
#, kde-format
msgid "Unlocking failed"
msgstr "Il falleva disblocar"

#: contents/lockscreen/LockScreenUi.qml:290
#, kde-format
msgid "Sleep"
msgstr "Dormi"

#: contents/lockscreen/LockScreenUi.qml:296 contents/logout/Logout.qml:210
#, kde-format
msgid "Hibernate"
msgstr "Hiberna"

#: contents/lockscreen/LockScreenUi.qml:302
#, kde-format
msgid "Switch User"
msgstr "Commuta Usator"

#: contents/lockscreen/LockScreenUi.qml:387
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "Commuta disposition"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "Disbloca"

#: contents/lockscreen/MainBlock.qml:156
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr "(o scande tu impression digital sur le lector)"

#: contents/lockscreen/MainBlock.qml:160
#, kde-format
msgid "(or scan your smartcard)"
msgstr "(o scande tu smarcard)"

#: contents/lockscreen/MediaControls.qml:59
#, kde-format
msgid "No title"
msgstr "Necun titulo"

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No media playing"
msgstr "Necun media reproducente"

#: contents/lockscreen/MediaControls.qml:88
#, kde-format
msgid "Previous track"
msgstr "Previe tracia"

#: contents/lockscreen/MediaControls.qml:100
#, kde-format
msgid "Play or Pause media"
msgstr "Reproduce o pone in pausa dispositivo de multimedia"

#: contents/lockscreen/MediaControls.qml:110
#, kde-format
msgid "Next track"
msgstr "Proxime tracia"

#: contents/logout/Logout.qml:151
#, kde-format
msgid "Installing software updates and restarting in 1 second"
msgid_plural "Installing software updates and restarting in %1 seconds"
msgstr[0] "Installante actualisationes de software e reinitiante in 1 secunda"
msgstr[1] ""
"Installante actualisationes de software e reinitiante in %1 secundas"

#: contents/logout/Logout.qml:152
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Arresta (shut down) in 1 secunda"
msgstr[1] "Reinitiante in %1 secundas"

#: contents/logout/Logout.qml:154
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Clausura de session in 1 secunda"
msgstr[1] "Clausura de session in%1 secundas"

#: contents/logout/Logout.qml:157
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Arresta (shut down) in 1 secunda"
msgstr[1] "Arresta (shut down) in %1 secundas"

#: contents/logout/Logout.qml:172
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"Un altere usator es currentemente connectite. Si le computator es claudite o "
"reinitialisate, celle usator poterea perder su labor."
msgstr[1] ""
"%1  altere usatores es currentemente connectite. Si le computator es "
"claudite o reinitialisate, celle usatores poterea perder su labor."

#: contents/logout/Logout.qml:187
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr ""
"Quando reinitialisate, le computator monstrara le schermo de configuration "
"del firmware."

#: contents/logout/Logout.qml:201
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep Now"
msgstr "Dormi Nunc"

#: contents/logout/Logout.qml:211
#, kde-format
msgid "Hibernate Now"
msgstr "Hiberna Nunc"

#: contents/logout/Logout.qml:222
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart"
msgstr "Installa Actualisationes & ReInitia"

#: contents/logout/Logout.qml:223
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart Now"
msgstr "Installa Actualisationes & ReInitia Nunc"

#: contents/logout/Logout.qml:226 contents/logout/Logout.qml:239
#, kde-format
msgid "Restart Now"
msgstr "Re-Initia nunc"

#: contents/logout/Logout.qml:252
#, kde-format
msgid "Shut Down Now"
msgstr "Stoppa (shut down) Nunc"

#: contents/logout/Logout.qml:262
#, kde-format
msgid "Log Out"
msgstr "Claude session"

#: contents/logout/Logout.qml:263
#, kde-format
msgid "Log Out Now"
msgstr "Claude session Nunc"

#: contents/logout/Logout.qml:273
#, kde-format
msgid "Cancel"
msgstr "Cancella"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1%"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "Plasma facite per KDE"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Switch to This Session"
#~ msgstr "Commuta a iste session"

#~ msgid "Start New Session"
#~ msgstr "Initia nove session"

#~ msgid "Back"
#~ msgstr "Retro"

#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery at %1%"
#~ msgstr "Batteria a  %1%"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "Non usate"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "TTY %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "sur TTY %1 (Monstrator %2)"

#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "100%"
#~ msgstr "100%"

#~ msgid "Configure"
#~ msgstr "Configura"

#~ msgid "Configure KRunner Behavior"
#~ msgstr "Configura le Comportamento de KRunner"

#~ msgid "Configure KRunner…"
#~ msgstr "Configura KRunner..."

#~ msgctxt "Textfield placeholder text, query specific KRunner"
#~ msgid "Search '%1'…"
#~ msgstr "Cerca '%1'..."

#~ msgctxt "Textfield placeholder text"
#~ msgid "Search…"
#~ msgstr "Cerca..."

#~ msgid "Show Usage Help"
#~ msgstr "Monstra adjuta de uso"

#~ msgid "Pin"
#~ msgstr "Pin"

#~ msgid "Pin Search"
#~ msgstr "Attacha cerca"

#~ msgid "Keep Open"
#~ msgstr "Manteni aperite"

#~ msgid "Recent Queries"
#~ msgstr "Requestas recente"

#~ msgid "Remove"
#~ msgstr "Remove"

#~ msgid "in category recent queries"
#~ msgstr "requestas (queries) recente in categoria"

#~ msgctxt "verb, to show something"
#~ msgid "Show:"
#~ msgstr "Monstra :"

#~ msgid "Configure Search Plugins"
#~ msgstr "Configura Plugins de cerca"

#~ msgctxt ""
#~ "This is the first text the user sees while starting in the splash screen, "
#~ "should be translated as something short, is a form that can be seen on a "
#~ "product. Plasma is the project name so shouldn't be translated."
#~ msgid "Plasma 25th Anniversary Edition by KDE"
#~ msgstr "Edition de anniversario 25  per KDE"

#~ msgid "Close"
#~ msgstr "Claude"

#~ msgctxt "verb, to show something"
#~ msgid "Always show"
#~ msgstr "Monstra sempre"

#~ msgid "Reboot"
#~ msgstr "Reboot (re-initia)"

#, fuzzy
#~| msgid "Password"
#~ msgid "Password..."
#~ msgstr "Contrasigno"

#~ msgid "Login"
#~ msgstr "Accesso de identification"

#, fuzzy
#~| msgctxt "Button to shut down the computer"
#~| msgid "Shutdown"
#~ msgid "Shutdown"
#~ msgstr "Stoppa (Shutdown)"

#~ msgid "New Session"
#~ msgstr "Nove session"

#, fuzzy
#~| msgid "%1\\%. Charging"
#~ msgid "%1%. Charging"
#~ msgstr "%1\\%. Cargante"

#~ msgid "Fully charged"
#~ msgstr "Completemente cargate"

#~ msgctxt "Button to restart the computer"
#~ msgid "Reboot"
#~ msgstr "Reboot (re-initia)"

#, fuzzy
#~| msgid "Shut down"
#~ msgctxt "Button to shut down the computer"
#~ msgid "Shut down"
#~ msgstr "Stoppa (shut down)"

#, fuzzy
#~| msgid "Reboot"
#~ msgctxt "Dialog heading, confirm reboot, not a status label"
#~ msgid "Rebooting"
#~ msgstr "Reboot (re-initia)"
